﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {
	public Button[] buttons;
	public GameObject[] highlight;
	public PlayerINput playerinput;
	public Quaternion rot;
	public Quaternion desiredRot;
	public GameObject rotatingObject;
	public GameObject musicStick;
	public GameObject SoundStick;
	bool music;
	bool sound;
	bool options;

	Quaternion angle;
	// Use this for initialization


	void Start(){
	
		angle.z = 360/(8-1);

	
	}

	void Update(){
		
		Vector2 inputdisplay = new Vector2 (Input.GetAxis (playerinput.LeftJoyStickX), Input.GetAxis (playerinput.LeftJoyStickY));

		if (options&&rotatingObject!=null) {


			if (inputdisplay.x != 0) {
				print("here");

				Quaternion.RotateTowards (rot, desiredRot, angle.z);
				rot = rotatingObject.transform.localRotation;
				desiredRot = rot * angle;
			

			}



			if (Input.GetAxis(playerinput.LeftJoyStickY)==-1) {

				music = false;
				sound = true;
				highlight [0].SetActive (true);
				highlight [1].SetActive (false);
				rotatingObject = SoundStick;
			}
			if (Input.GetAxis(playerinput.LeftJoyStickY)==1) {

				music =true;
				sound = false;
				highlight [1].SetActive (true);
				highlight [0].SetActive (false);
				rotatingObject = musicStick;

			}
		
		}

	}
	// Update is called once per frame
	public void PlayButton(){


		SceneManager.LoadScene ("Level_Chris");

	}
	public void optionButton(){
		buttons[0].GetComponent<Button> ().enabled = false;
		buttons[1].GetComponent<Button> ().enabled = false;
		highlight [1].SetActive (true);
		music = true;
		options = true;
		rotatingObject = musicStick;

		rot = rotatingObject.transform.localRotation;
		desiredRot = rot * angle;
	

	
	}
	void rotating(){
		
	}
}
