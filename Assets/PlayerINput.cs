﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerINput : MonoBehaviour {

	int playerIndex;

	public enum playerSlot
	{
		player1,player2
	};
	public playerSlot player;

	public string LeftJoyStickX;
	public string LeftJoyStickY;
	public string RightJoyStickX;
	public string RightJoyStickY;

	public string A;
	public string B;
	public string Y;
	public string X;

	public string RightBumper;
	public string LeftBumper;

	public string LeftTrigger;
	public string RightTrigger;

	public string LeftJoyStickButton;
	// Use this for initialization
	void Start () {

		if (player == playerSlot.player1) {
		
			playerIndex = 1;
		
		} else {
		
			playerIndex = 2;
		
		}

		LeftJoyStickX = "LeftJoyStickX";
		LeftJoyStickY="LeftJoyStickY";
		RightJoyStickX="RightJoyStickX";
		RightJoyStickY="RIghtJoyStickX";

		A = "A" ;
		B = "B";
		X = "X" ;
		Y = "Y" ;

		RightBumper = "RightBumper" ;
		LeftBumper = "LeftBumper" ;

		RightTrigger = "RightTrigger" ;
		LeftTrigger = "LeftTrigger" ;

		LeftJoyStickButton = "LeftJoyStickButton" ;
	}
	
	// Update is called once per frame

}
