﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackController : MonoBehaviour {



	public float attackTimer = 0;
	public int weaponIndex=0;
	Bounds bounds;
	Vector2 colliderSize;
	Vector2 oldColliderSize;
	Vector2 newColliderSize;

	public Quaternion playerRotation;

	Vector2 colliderOffset;
	Vector2 newColliderOffset;
	Vector2 oldColliderOffset;
	public GameObject bulletSpawner;
	public GameObject bullets;
	public Vector2 parentVelocity;
	public bool isAttacking;
	public Animator anim;

	public LayerMask attackMask;

	public Vector2 shaker;
	public Vector2 input;
	public GameObject weapon;

	public GameObject Character;
	[Range(0,20)]
	public int myDamage;
	public PlayerINput playerInput;
	float angleZ;
	float angleY;
	Quaternion rot;
	public int ammunition =1000;
	public Vector3 pos;
	public Vector2 size;
	// Use this for initialization
	void Start () {
		


		shaker = bulletSpawner.transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
		//inputs
		weaponIndex= Mathf.Clamp(weaponIndex,0,1);
		if(Input.GetButtonDown(playerInput.LeftBumper)){

			weaponIndex--;
		}else if (Input.GetButtonDown(playerInput.RightBumper)){

			weaponIndex++;
		}

		if (Input.GetAxis(playerInput.LeftJoyStickX)<0) {

		    rot = this.transform.localRotation;
			rot.y = 180;
			Character.transform.localRotation = rot;
			bulletSpawner.transform.localRotation = rot;

	
		}else if ( Input.GetAxis(playerInput.LeftJoyStickX)>0){

			rot = this.transform.localRotation;
			rot.y = 0;
			Character.transform.localRotation = rot;
			bulletSpawner.transform.localRotation = rot;

			}
		if (Input.GetAxis(playerInput.LeftJoyStickY)!=0) {
			float dir = Mathf.Sign (Input.GetAxis(playerInput.LeftJoyStickY));
			dir = -dir;
			weapon.transform.localRotation = Quaternion.Euler(0,Character.transform.localRotation.y,45*dir);
			angleZ = 45*dir ;


		}else if (Input.GetAxis(playerInput.LeftJoyStickY)==0){

			angleZ=0;
			weapon.transform.localRotation = Quaternion.Euler(0,Character.transform.localRotation.y,0);
		}
		bulletSpawner.transform.localRotation = Quaternion.Euler(0,-rot.y,angleZ);






	



		if (Input.GetButtonDown(playerInput.X)&&!isAttacking) {
			isAttacking = true;
			if(weaponIndex ==0){
				anim.SetTrigger("melee0");
				meleeAttack ();

			}else if (weaponIndex==1){

				ShootAttack ();
			}
				

			



		}

	

		if (isAttacking) {
			if (attackTimer > 0) {
				attackTimer-=Time.deltaTime;

			}if (attackTimer <= 0) {
				

				isAttacking = false;

				attackTimer = 0;
			}
		}




		
	}

	void meleeAttack(){
		RaycastHit2D attack = Physics2D.BoxCast (this.transform.position+pos, size, 0, Vector2.right, 0, attackMask);
		if (attack) {
			print ("JO");
			attack.transform.GetComponent<Damage>().underAttack(myDamage);
		
		}
		attackTimer = 0.2f;

	}
	void OnDrawGizmosSelected(){
	
		Gizmos.color = Color.green;
		if (isAttacking) {
			Gizmos.DrawWireCube (this.transform.position + pos, size);
		}

	
	}
	void ShootAttack(){
		
	

		StartCoroutine (delay());

		
		attackTimer = 0.2f;
	
	}

	void OnTriggerEnter2D(Collider2D col) {
	
		Destroy (col.gameObject);
	
	}
	IEnumerator delay(){


			

			Instantiate (bullets, bulletSpawner.transform.position, bulletSpawner.transform.localRotation);
	
			yield return new WaitForSeconds(0.1f);





	}

}
