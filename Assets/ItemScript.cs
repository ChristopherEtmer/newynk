﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemScript : MonoBehaviour {


	public Vector2 size;
	public Vector2 pulling;
	public bool connected;
	public LayerMask collisionMask;
	public LayerMask collectionMask;
	public Vector2 distance;
	public float dir;
	public float moveSpeed;
	// Use this for initialization
	public GameObject item;
	public Player controller;
	public PlayerINput playerInput;

	public GameObject buttonDisplay;
	public int keys;
	public Vector3 pos;
	public GameObject player;
	void Start () {
		
		
	}
	void Awake(){
		
	
	}
	
	// Update is called once per frame
	void Update () {

		dir = Input.GetAxis (playerInput.LeftJoyStickX);

	
		if (connected) {
		
			pull ();

		}
		RaycastHit2D hit = Physics2D.BoxCast(this.transform.position,size,0,Vector2.right*Mathf.Sign(dir),0,collisionMask);
		RaycastHit2D hitGround = Physics2D.BoxCast(player.transform.position+pos,size,0,Vector2.right,0,collectionMask);
		if (hitGround) {
		
			Destroy (hitGround.transform.gameObject);
			keys++;
		
		}
		if (hit) {
			buttonDisplay.transform.FindChild ("B").gameObject.SetActive (true);
		} else {
		
			buttonDisplay.transform.FindChild("B").gameObject.SetActive (false);
		
		}
		if (Input.GetButtonDown (playerInput.A)) {
			if (connected) {
			
				item.gameObject.layer = 16;
				item.GetComponent<Controller>().collisionMask^= (1 << LayerMask.NameToLayer("Item"));
				connected = false;

				item = null;
			
			}
		}

		if (Input.GetButtonDown (playerInput.B)) {
			
			if (hit) {

				if (hit.transform.gameObject.layer == 16) {
				
					if (!connected) {
						
							connected = true;
							item = hit.transform.gameObject;

					}
				
				}
			}else  {
				
				if (item != null) {
					item.gameObject.layer = 16;
					item.GetComponent<Controller>().collisionMask^= (1 << LayerMask.NameToLayer("Item"));
					connected = false;

					item = null;
				}



			}
	}
	}

	


		
		

	
		//Debug.DrawRay (this.transform.position, pulling, Color.blue);

	void OnDrawGizmosSelected(){

		Gizmos.color = Color.red;
		Gizmos.DrawWireCube (this.transform.position, size);
		Gizmos.DrawWireCube (player.transform.position+pos, size);

	}
	void pull(){
		
		if (item!= null) {
			item.gameObject.layer = 17;
			item.GetComponent<Controller>().collisionMask|= (1 << LayerMask.NameToLayer("Item"));

			Vector2 velocity= new Vector2 (controller.velocity.x*Time.deltaTime, 0);
			item.GetComponent<Controller>().move(velocity);
		}

			


	}
}
