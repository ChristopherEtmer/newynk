﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthScript : MonoBehaviour {

    public float health;
	public GameObject healthDisplay;
	public Vector2 healthBarSize;
	public Vector2 barPos;
	public Color color;
	public Sprite spriteSize;
	public GameObject mask;
	Vector2 pos;

	// Use this for initialization
	public virtual void  Start () {
		
		if (healthDisplay != null) {
		
			healthBarSize.x = healthDisplay.GetComponent<RectTransform> ().sizeDelta.x;
			healthBarSize.y = healthDisplay.GetComponent<RectTransform> ().sizeDelta.y;
			health =  healthDisplay.GetComponent<RectTransform> ().sizeDelta.x;
			barPos.y = healthDisplay.GetComponent<RectTransform> ().localPosition.y;
			mask = GameObject.Find ("Canvas").GetComponent<CanvasManager> ().Mask2;
		}


	}
	public virtual void Awake(){
	
	
	}

	public void takeDamageObstacle(float Damage){

		health = health - Damage;
		pos = mask.transform.localPosition;
		pos.x = pos.x - Damage;

	
	}
	public void takeDamagePlayer (int Damage) {

		health = health - Damage;
		pos = mask.transform.localPosition;
		pos.x += 50;
		mask.transform.localPosition = pos;

	}

	public void takeDamage (int Damage) {
		if (healthBarSize.x > 0) {
			health = health - Damage;
			healthBarSize.x = healthBarSize.x - Damage;
			healthDisplay.GetComponent<RectTransform> ().sizeDelta = new Vector2 (healthBarSize.x, healthBarSize.y);
			barPos.x = healthDisplay.GetComponent<RectTransform> ().localPosition.x;
			healthDisplay.transform.localPosition = new Vector2 (barPos.x - Damage, barPos.y);
		}
	}
}
