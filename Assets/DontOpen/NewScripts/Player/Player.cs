﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : RayCastController {


	public Controller controller;

	public Vector2 velocity;
	public float smoothTime;
	public float smoothingX;
	[Range(0,0.1f)]
	public float smoothingY;
	[Range(0,0.1f)]
	public float smoothTimeY;
	[Range(0,50)]
	public float jumpHeight;
	[Range(0,2)]
	public float jumpDuration;
	public float jumpVelocity;
	public float gravity ;
	[Range(0,20)]
	public float moveSpeed ;
	public float runningSpeed;
	public Vector2 input;
	public float oldMoveSpeed;

	public float maxHearingRadius;
	public CircleCollider2D noiseCollider;
	public float hearingRadius;
	bool running;
	bool crouching;
	public Animator anim;
	public Vector2 display;
	public GameObject sprite;
	Vector3 spriteSize;
	Vector3 newSpriteSize;
	// Use this for initialization
	public override void Start () {
		
		base.Start ();
	}
	void Awake(){
		spriteSize = sprite.transform.localScale;
		newSpriteSize= sprite.transform.localScale;
		newSpriteSize.y = spriteSize.y * 1.3f;
		oldMoveSpeed = moveSpeed;
		anim = GetComponentInChildren<Animator> ();
		hearingRadius = Mathf.Clamp (hearingRadius, 0, maxHearingRadius);
	}
	// Update is called once per frame
	void Update () {

		display.x = Input.GetAxis ("LeftTrigger");
		display.y = Input.GetAxis ("RightTrigger");

		if (display.x != 0 ) {
			
			running = true;
			
		} else {
		
			running = false;
		
		}
		if (running && !crouching) {
		
			moveSpeed = runningSpeed;
		
		} else if (!running && !crouching) {
		
			moveSpeed = oldMoveSpeed;
		
		}
		

		if(Input.GetButtonDown("LeftJoyStickButton")&&controller.collisions.below){
			bool crouch = anim.GetBool ("crouching");

			controller.crouch();
			if (crouch ==true) {
				;
				sprite.transform.localScale=spriteSize;
				crouching = false;
				moveSpeed = oldMoveSpeed;
				anim.SetBool ("crouching", false);

			} else {
				sprite.transform.localScale=newSpriteSize;
				crouching = true;
				moveSpeed = moveSpeed / 2;
				anim.SetBool ("crouching", true);
			
			}
		}

		gravity = -(2 * jumpHeight) / Mathf.Pow (jumpDuration, 2);
		jumpVelocity = Mathf.Abs (gravity) * jumpDuration*Time.deltaTime;

		if (controller.collisions.below || controller.collisions.above) {
		
			velocity.y = 0;
			anim.SetBool ("grounded", true);
			anim.SetBool ("jumping", false);
			anim.SetBool ("falling", false);
		
		} else {
			anim.SetBool ("grounded", false);
		
		}
		if (Input.GetButtonDown("A") && controller.collisions.below) {
			
			anim.SetBool ("jumping", true);
			velocity.y = jumpHeight;
			anim.SetBool ("falling", true);
		
		}
		if (velocity.y < -2) {
		
			anim.SetBool ("falling", true);

		}

		input=new Vector2(Input.GetAxis("LeftJoyStickX"),Input.GetAxis("Vertical"));
		float targetVelocityx=input.x*moveSpeed;

	
		hearingRadius = (maxHearingRadius)*Mathf.Abs(input.x);
		noiseCollider.radius = hearingRadius;
		if (controller.icy) {
		
			smoothTime = 2;

		} else {
		
			smoothTime = 0.002f;

		}

		velocity.x = Mathf.SmoothDamp (velocity.x, targetVelocityx, ref smoothingX, smoothTime);
	
		float targetVelocityY = velocity.y + gravity*Time.deltaTime;
		velocity.y = Mathf.SmoothDamp (velocity.y, targetVelocityY, ref smoothingY, smoothTimeY);


		controller.move (velocity*Time.deltaTime);


		if (input.x != 0) {
			anim.SetBool ("walking", true);


		} else {
		
			anim.SetBool ("walking", false);
		
		}
	

		
	}
}
