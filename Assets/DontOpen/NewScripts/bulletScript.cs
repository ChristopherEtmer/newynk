﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletScript : MonoBehaviour {
	public float penetrationTime =10f;
	public int speed = 80;
	//public float dir;
	//public GameObject Player;
	public Vector2 velocity;
	public LayerMask collisionMask;
	bool drawBlood;
	[Range (0,50)]
	public int myDamage;

	public Vector2 size;
	// Use this for initialization
	void Start () {
		

	}

	// Update is called once per frame
	void Update () {
		RaycastHit2D hit = Physics2D.BoxCast (this.transform.position, size, 0, Vector2.right, 0, collisionMask);
		if (hit&&!drawBlood) {
			drawBlood = true;
			if (hit.transform.gameObject.GetComponent<Damage> () != null) {
			
				hit.transform.gameObject.GetComponent<Damage> ().underAttack (myDamage);
				Destroy (this.transform.gameObject);
		}

			}



		velocity.x = speed * Time.deltaTime;

		transform.Translate (velocity);
		StartCoroutine (objectDuration());


		
	}
	void OnDrawGizmosSelected(){

		Gizmos.color = Color.red;
		Gizmos.DrawWireCube (this.transform.position, size);

	}
	IEnumerator objectDuration(){
		yield return new WaitForSeconds (3f);
		Destroy (this.gameObject);
		StopCoroutine(objectDuration());
	}

}
