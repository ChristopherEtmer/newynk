﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {
	public GameObject SpawnObject;
	// Use this for initialization
	void Start () {
		Instantiate (SpawnObject, this.transform.position, Quaternion.identity);
	}
	
	// Update is called once per frame

}
