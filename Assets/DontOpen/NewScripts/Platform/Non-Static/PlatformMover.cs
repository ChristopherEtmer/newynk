﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMover : MonoBehaviour {

	public PlatformController controller;

    public Vector2 velocity;
	[Tooltip("Platformspeed")]
	[Range (0,50)]
	public float movingSpeed;

	[Tooltip("distance the platform travels from its parent location(see blue box in scene view)")]
	[Range (0,50)]
	public float Amplitude;

	public float diagonalamplitude;

    Vector3 originalPos;
    Vector3 distance;
	public Vector3 diagonalAmplitude;
	Vector3 circleRadius;

	public enum Direction{shuttleHorizontally,shuttleVertically,shuttleDiagonally,circling,notMoving };

	[Tooltip("The Direction which the platform is traveling")]
	public Direction moveDirection;


	public enum DiagonalMovement{ positive,negative };
	[Tooltip("The starting direction of the moving platform")]
	public DiagonalMovement diagonalDirection;

	public enum CirclingDirection{ clockwise,counterclockwise };
	public CirclingDirection circlingDirection;

	public int diagonal;
	public GameObject Sprite;

	public float circleAngle=0;
	[Tooltip("the radius on which the platform travels(allign Platform on blue circle to make it move on it)")]
	public float radius;
	float startingAngle;
	[Tooltip("Time the platform needs to go once around the circle")]
	public float circulationTime ;
	public float radiantFrequency;
	float timer;

	// Use this for initialization


	void Start () {

		originalPos = transform.parent.position;
		Amplitude = Mathf.Clamp (Amplitude, 0, 40);
		diagonal= Mathf.Clamp (diagonal, -1, 1);

		radius = Vector3.Distance (this.transform.position, Sprite.transform.position);
		startingAngle = Vector2.Angle (this.transform.localPosition, Vector2.right);
		circulationTime = radius * (2 * Mathf.PI) / movingSpeed;
		radiantFrequency = (2 * Mathf.PI) / circulationTime;

		if (circlingDirection == CirclingDirection.counterclockwise) {

			startingAngle = -startingAngle;

		}

		if (Mathf.Sign (this.transform.localPosition.y) == -1) {


			startingAngle = -startingAngle;
		}
	}
	
	// Update is called once per frame
	void FixedUpdate(){

		if (moveDirection==Direction.circling) {

			swinging (ref velocity);

		}
	}
	void Update () {

		if (moveDirection != Direction.notMoving) {
			move (ref velocity);
		}

		controller.move (velocity*Time.deltaTime);

	
	}

	void move(ref Vector2 velocity)
	{


		if (diagonalDirection== DiagonalMovement.positive) {
			diagonal= 1;
		} else {
			diagonal= -1;
		}
	

		if (moveDirection==Direction.shuttleHorizontally) {

			horizontalMovement (ref velocity);

		}

		if (moveDirection==Direction.shuttleVertically) {

			verticalMovement (ref velocity);

		}

		if (moveDirection==Direction.shuttleDiagonally) {

			diagonalMovement (ref velocity);

		}


	}	void horizontalMovement(ref Vector2 velocity){
		if(controller.horizontalCol){

			movingSpeed=-movingSpeed;

		}
		if (this.transform.position.x >= originalPos.x + Amplitude) {
			this.transform.position = new Vector2(originalPos.x+Amplitude,this.transform.position.y);
			movingSpeed = -movingSpeed;
		} else if (this.transform.position.x <= originalPos.x - Amplitude) {
			this.transform.position = new Vector2(originalPos.x-Amplitude,this.transform.position.y);
			movingSpeed = -movingSpeed;
		}
		velocity.x = movingSpeed;
		velocity.y =0;
	}

	void verticalMovement(ref Vector2 velocity){

		if (controller.verticalCol) {
		
			movingSpeed = -movingSpeed;
				
				}
		if(this.transform.position.y > originalPos.y+Amplitude)
		{
			this.transform.position = new Vector2(this.transform.position.x,originalPos.y+Amplitude);
			movingSpeed=-movingSpeed;
		}else if 
			(this.transform.position.y <= originalPos.y - Amplitude) {
			this.transform.position = new Vector2(this.transform.position.x,originalPos.y-Amplitude);
			movingSpeed = -movingSpeed;
		}
		velocity.y = movingSpeed;
	}

	void diagonalMovement(ref Vector2 velocity){
		
		distance =this.transform.position-originalPos;

		diagonalAmplitude= new Vector3((Mathf.Sin (45 * Mathf.Deg2Rad))*distance.magnitude,(Mathf.Cos(45*Mathf.Deg2Rad)*distance.magnitude));
		diagonalAmplitude.x = diagonalAmplitude.x* diagonal;

		if(distance.magnitude >=Amplitude  )
		{
			if(Mathf.Sign(velocity.y)==1){
				this.transform.position = originalPos + diagonalAmplitude;
			}else{
				this.transform.position = originalPos - diagonalAmplitude;

			}

			movingSpeed=-movingSpeed;
		}

		velocity.y = Mathf.Sin (45 * Mathf.Deg2Rad) * movingSpeed;
		velocity.x = Mathf.Cos (45 * Mathf.Deg2Rad) * movingSpeed*diagonal;

	}

	void swinging(ref Vector2 velocity){


		
			timer += Time.fixedDeltaTime;
		
		


		circleAngle=(radiantFrequency*timer)*Mathf.Rad2Deg ;

	
	
	
		velocity.x = -Mathf.Sin ((circleAngle+(startingAngle)) * Mathf.Deg2Rad) * movingSpeed;
		velocity.y=  Mathf.Cos ((circleAngle+(startingAngle)) * Mathf.Deg2Rad) * movingSpeed;

	

		Vector2 counterVelocity = new Vector2(velocity.x,-velocity.y);

		//Debug.DrawRay (this.transform.position, velocity, Color.blue);

		//Debug.DrawRay (this.transform.position, counterVelocity, Color.red);

		if (circlingDirection == CirclingDirection.counterclockwise) {
		
			velocity = counterVelocity;

		}

		radius = Vector3.Distance (this.transform.position, gameObject.transform.parent.position);

		//Debug.DrawRay (new Vector3 (0, 0, 0), velocity, Color.green);


	}

	void OnDrawGizmosSelected(){
		Gizmos.color = Color.cyan;
	
		if (moveDirection==Direction.circling) {
			
			Gizmos.DrawWireSphere (transform.parent.position, radius);

		}

		if (moveDirection==Direction.shuttleHorizontally) {

			Gizmos.DrawWireCube (transform.parent.position, new Vector2 (2*Amplitude+(this.transform.localScale.x), this.transform.localScale.y));

		}

		if (moveDirection==Direction.shuttleVertically) {

			Gizmos.DrawWireCube(transform.parent.position, new Vector2 ( 1,2*Amplitude));


		}

		if (moveDirection==Direction.shuttleDiagonally) {
			Vector2 diagonal;
			diagonal.x = Mathf.Cos (45 * Mathf.Deg2Rad) * Amplitude;
			diagonal.y = Mathf.Sin (45 * Mathf.Deg2Rad) *  Amplitude;

			Gizmos.DrawRay(gameObject.transform.parent.position,diagonal);
			Gizmos.DrawRay(gameObject.transform.parent.position,-diagonal);


		

		}

	}
}


