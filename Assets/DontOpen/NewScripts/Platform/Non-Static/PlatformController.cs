﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformController : RayCastController {

	public bool horizontalCol;
	public bool verticalCol;
	public LayerMask passengerMask;
	public LayerMask mask;
	public float globalDirectionX;
	public float globalDirectionY;
	public GameObject player;
	public ObstacleCollision obstacle;
	public bool colliding;
	public Controller controller;
	// Use this for initialization
	public override void Start () {
		base.Start ();

	}
	
	// Update is called once per frame
	public void move(Vector2 velocity){
		RaycastUpdateOrigin ();
	


		MovePassengers (velocity);
		


		horizontalCollision (ref velocity);
		verticalCollision (ref velocity);

		transform.Translate (velocity);


	}

	void MovePassengers(Vector2 velocity){
		HashSet<Transform> movedPassengers = new HashSet<Transform> ();
		float directionX = Mathf.Sign (velocity.x);
		float directionY = Mathf.Sign (velocity.y);

		if (velocity.y != 0) {
			float rayLength = Mathf.Abs (velocity.y) + skinWidth;

			for(int i =0; i <horizontalRayCount;i++){

				Vector2 rayOrigin = (directionY==-1)?raycastOrigin.bottomLeft:raycastOrigin.topLeft;
				rayOrigin += Vector2.right * (horizontalRaySpacing*i);
				RaycastHit2D hit = Physics2D.Raycast(rayOrigin,Vector2.up*directionY,rayLength, passengerMask);
				//Debug.DrawRay (rayOrigin, Vector2.up * directionY, Color.blue);


				if(hit){
					
					if(!movedPassengers.Contains(hit.transform)){

						movedPassengers.Add(hit.transform);
						float pushX=(directionY==1)?velocity.x:0;
						float pushY=velocity.y -(hit.distance-skinWidth)*directionY;

						hit.transform.Translate(new Vector2 (pushX,pushY));

					}
				}
			}
		}
		if ( velocity.x != 0) {
			float rayLength = Mathf.Abs (velocity.x) + skinWidth;

			for(int i =0; i <verticalRayCount;i++){
				
				Vector2 rayOrigin = (directionX==-1)?raycastOrigin.bottomLeft:raycastOrigin.bottomRight;
				rayOrigin += Vector2.up * (verticalRaySpacing*i)+Vector2.down*0.2f;

				RaycastHit2D hit = Physics2D.Raycast(rayOrigin,Vector2.right *directionX,rayLength, passengerMask);
				//Debug.DrawRay (rayOrigin, Vector2.right * directionX, Color.blue);


				if(hit){
					


						movedPassengers.Add(hit.transform);
						float pushX = velocity.x -(hit.distance-skinWidth)*directionX;
						float pushY = 0;
				     	
						hit.transform.Translate(new Vector2 (pushX,pushY));

					}

				}
			}

		if (directionY==-1|| velocity.y ==0 &&velocity.x != 0) {
			float rayLength = 7*skinWidth;

			for(int i =0; i <horizontalRayCount;i++){

				Vector2 rayOrigin =raycastOrigin.topLeft+Vector2.right * (horizontalRaySpacing*i+velocity.x);
		
				RaycastHit2D hit = Physics2D.Raycast(rayOrigin,Vector2.up,rayLength, passengerMask);
				//Debug.DrawRay (rayOrigin, Vector2.up *4, Color.blue);


				if (hit) {
					//Debug.DrawRay (rayOrigin, Vector2.up*rayLength*10, Color.red);

					controller = hit.transform.gameObject.GetComponent<Controller> ();

					if (!movedPassengers.Contains (hit.transform)) {
						
						controller.horizontalCollision (ref velocity);
						movedPassengers.Add (hit.transform);
						float pushX = velocity.x;
						float pushY = velocity.y;

						if (!controller.collisions.left) {
							if (!controller.collisions.right) {
								hit.transform.Translate (new Vector2 (pushX, pushY));
							}

							//controller.horizontalCollision (ref velocity);
						}
					}
				} else {
				
					controller = null;
				
				}
			}
		}

	
	}
	public void verticalCollision(ref Vector2 velocity){

		float directionY = Mathf.Sign (velocity.y);
		float raylength = Mathf.Abs (velocity.y) + skinWidth;

		for(int i=0;i<horizontalRayCount;i++){

			Vector2 rayOrigin=(directionY==-1)?raycastOrigin.bottomLeft:raycastOrigin.topLeft;
			rayOrigin+=Vector2.right *(horizontalRaySpacing*i);

			RaycastHit2D hit = Physics2D.Raycast(rayOrigin,Vector2.up*directionY,raylength,collisionMask); 
		//	Debug.DrawRay (rayOrigin, Vector2.up * directionY*raylength, Color.cyan);
			if (hit) {



				verticalCol = true;

			} else {

			verticalCol = false;

			}
		}
	}
	public void horizontalCollision(ref Vector2 velocity){

		float directionX = Mathf.Sign (velocity.x);
		float raylength = Mathf.Abs (velocity.x) + skinWidth;

		for(int i=0;i<verticalRayCount;i++){

			Vector2 rayOrigin=(directionX==-1)?raycastOrigin.bottomLeft:raycastOrigin.bottomRight;
			rayOrigin+=Vector2.up *(verticalRaySpacing*i);

			RaycastHit2D hit = Physics2D.Raycast(rayOrigin,Vector2.right*directionX,raylength,collisionMask); 
			//Debug.DrawRay (rayOrigin, Vector2.right * directionX*raylength, Color.cyan);
			if (hit) {

				horizontalCol = true;


			} else {
			
				horizontalCol = false;
			
			}
		}
	}

}
