﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerBulletSpawner : MonoBehaviour {

	// Use this for initialization
	public GameObject bulletSpawner;

	public GameObject bullets;
	public float shootingFrequency;
	float timer;

	void Start (){

		timer = shootingFrequency;
		Quaternion rot = this.transform.localRotation;
		bulletSpawner.transform.localRotation = rot;

	}
	
	// Update is called once per frame
	void Update () {

		timer -= Time.deltaTime;
		if (timer <= 0) {
			Instantiate (bullets, bulletSpawner.transform.position, bulletSpawner.transform.localRotation);
			timer = shootingFrequency;
		}

		
	}

}
