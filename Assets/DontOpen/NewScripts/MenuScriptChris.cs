﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuScriptChris : MonoBehaviour {

	// Use this for initialization
	bool open;
	public Image musicSwitch;
	public Image soundSwitch;
	public GameObject screen;
	public Image musicHalo;
	public Image soundHalo;
	public Button[] buttonArray;
	public bool switches;
	public Image currentSwitch;
	public Image currentHalo;
	Quaternion rot;
	public GameObject player;


	void Start(){

		if (player == null) {
		
			player = GameObject.FindGameObjectWithTag ("Player");
		
		}

	}
	// Update is called once per frame
	void Update () {
		
		if (currentSwitch != null) {
			

			if (Input.GetButtonDown ("B")) {
					
				currentSwitch = null;
				switches = false;
				currentHalo.gameObject.SetActive (false);
				ButtonStop ();

			} else {

				Vector2 input = new Vector2 (Input.GetAxisRaw ("LeftJoyStickX"), Input.GetAxisRaw ("LeftJoyStickY"));
				rot = currentSwitch.transform.localRotation;
				rot.z = currentSwitch.transform.localRotation.z +input.x*0.1f;
				currentSwitch.transform.localRotation = rot;
			}


		}

		


		if (Input.GetButtonDown("Start")){
			if (!open) {
			
				Time.timeScale = 0;
				player.gameObject.SetActive (false);
			
			}else{
				player.gameObject.SetActive (true);
				Time.timeScale=1;

			}
			if (!open) {
			
				open = true;
				screen.gameObject.SetActive (true);
			
			} else {

				open = false;
				screen.gameObject.SetActive (false);
			}

		}
	}


	

	public void Continue(){
		Time.timeScale=1;
		player.gameObject.SetActive (true);
		open = false;
		screen.gameObject.SetActive (false);
	

	
	
	}
	public void Music(){
		musicHalo.gameObject.SetActive (true);
		currentHalo = musicHalo;
		currentSwitch = musicSwitch;
		switches = true;
		ButtonStop ();

	}
	public void Sound(){
		soundHalo.gameObject.SetActive (true);
		currentHalo = soundHalo;
		currentSwitch = soundSwitch;
		switches = true;
		ButtonStop ();

	}
	void ButtonStop(){

		for (int i = 0; i < buttonArray.Length; i++) {
			if (switches) {
				buttonArray [i].GetComponent<Button> ().enabled = false;
			} else {
				buttonArray [i].GetComponent<Button> ().enabled = true;
			}
		
		
		
		}

	}

}
