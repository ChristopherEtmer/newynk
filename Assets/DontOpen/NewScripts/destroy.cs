﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class destroy : HealthScript {
	public int pic=1;
	public LayerMask bodyMask;
	bool hit;
	public Vector2 size;

	public float enemyHealth;
	bool tookDamage;

	public enum objectType
	{
		Obstacle,Enemy,Player
	};

	public objectType type;

	public Animator anim;
	public float startHealth;
	public Sprite[] ObstacleSprites;

	// Use this for initialization
	public override void Start () {
		base.Start ();
		if (type == objectType.Obstacle) {
			size.x = spriteSize.bounds.size.x * this.transform.localScale.x;
			size.y = spriteSize.bounds.size.y * this.transform.localScale.y;

		}
		startHealth = health;
	}
	public override void Awake () {
		base.Awake ();

	}



	
	
	// Update is called once per frame
	void Update () {
		
		enemyHealth = health;
	
		if (enemyHealth <= 0) {
		
			Destroy (this.gameObject);
		
		}
		hit = Physics2D.OverlapBox (this.transform.position,size, 0, bodyMask);
		if(hit){
			if(!tookDamage){
				
				tookDamage=true;
				if (type == objectType.Enemy) {
					
					takeDamage (10);
					anim.SetTrigger ("Damage");
				
				}
				if (type == objectType.Player) {
					takeDamagePlayer (10);
					anim.Play ("dudeDamage");
				}
				if (type == objectType.Obstacle) {
					takeDamageObstacle (10f);
					if (enemyHealth == startHealth - 10/30/50) {

					
						pic++;
						ObstacleAnim (pic-1);
					}
				

				}

			}

			}
		if (!hit) {
		
			tookDamage = false;
		
		}
		

	}

	void OnDrawGizmosSelected(){

		Gizmos.color = Color.green;
		Gizmos.DrawWireCube (this.transform.position, size);


	}
	void ObstacleAnim(int Index){


		for (int i = 0; i < ObstacleSprites.Length; i++) {
		
		
			if (Index == i) {
			
				GetComponentInChildren<SpriteRenderer>().sprite= ObstacleSprites[i]; 
			
			}
		
		}

	}

}
