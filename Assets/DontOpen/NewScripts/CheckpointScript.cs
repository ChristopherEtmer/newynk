﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointScript : CheckpointController {

	public bool activated;
	bool playerHit;
	public Vector2 size;
	public LayerMask playerMask;
	public bool destroying;



	// Use this for initialization
	void Start(){



	}
	
	// Update is called once per frame
	void Update () {
		
		CheckpointManager ();
		if (Player == null) {
		
			Respawn ();
		
		}

		playerHit= Physics2D.OverlapBox(this.transform.position,size,0,playerMask);
		if (this.gameObject.tag == "Untagged") {

			activated = false;

		}

		if (playerHit) {
			if (!activated) {

				activated = true;
				this.gameObject.tag = "currentCheckpoint";
				if (lastCheckpoint != null) {
					lastCheckpoint.tag = "Untagged";
				}


			}

		}


	}
	void OnDrawGizmos(){

		if (activated) {
		
			Gizmos.color = Color.green;	
		} else {
		
			Gizmos.color = Color.red;	
		
		}
		Gizmos.DrawWireCube (this.transform.position, size);
	}
	public void Respawn(){


		if (activated) {
			print ("gone");
			Instantiate (playerPrefab, this.transform.position, Quaternion.identity);


		}

	}
}
