﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformBehaviour : RayCastController {

	// Use this for initialization
	public LayerMask passengerMask;
	public enum Behaviour{bouncy,falling,icy, none};
	public Behaviour behaviour;
	public float fallingTime;
	public bool onPlatform= false;
	public BoxCollider2D box;
	float originalFallingtime;



	public float smoothingY;
	public float smoothingTimeY;


	[Range (0,100)]
	public float jumpHeight;
	public float jumpDuration;


	[Range (0,5)]
	public float fallingSpeed;
	Vector2 currentPos;
	Vector2 oldPos;

	public Vector2 velocity;
	public float current;
	public float targetVelocity;
	public float smoothing;

	// Update is called once per frame


	void Awake(){
	
		if (behaviour == Behaviour.icy) {
		
			this.gameObject.tag = "icy";
		
		}

		originalFallingtime= fallingTime;
		oldPos = this.transform.position;

	}
	public override void Start () {
		base.Start ();


	}


	void Update () {
		RaycastUpdateOrigin ();

		RaySpacing ();
		DrawRays ();

		currentPos = this.transform.position;
		if (currentPos.y <= oldPos.y - 10) {
		

			StartCoroutine (resetPlatform ());
		
		}



		if (behaviour == Behaviour.falling) {
			if (!onPlatform) {
				if (behaviour == Behaviour.falling) {
					if (fallingTime < originalFallingtime) {
			
						fallingTime -= Time.deltaTime;
					}
				}

			}
			if (onPlatform) {
				fallingTime -= Time.deltaTime;
			}
			if (fallingTime < 0) {
		
				transform.Translate (Vector2.down * Time.deltaTime * fallingSpeed);


			}
		}
	}
	void Bouncing (GameObject tossing){
		RaycastUpdateOrigin ();
	
		
		if (tossing.transform.gameObject.layer == 9) {
			
			tossing.transform.GetComponent<Player> ().velocity.y = jumpHeight;
			print ("playerJump");
		}
		if (tossing.transform.gameObject.layer == 10) {

			tossing.transform.GetComponent<EnemyMover> ().velocity.y = jumpHeight;

		}
	
	}
	void DrawRays(){
		
		float raylength =(behaviour==Behaviour.bouncy)?0.2f+skinWidth: 1+ skinWidth;
		for (int i = 0; i < horizontalRayCount; i++) {
		
			Vector2 rayOrigin = raycastOrigin.topLeft + Vector2.right * i * horizontalRaySpacing;

			RaycastHit2D hit = Physics2D.Raycast (rayOrigin, Vector2.up, raylength, passengerMask);
			Debug.DrawRay (rayOrigin, Vector2.up * raylength, Color.red);

			if (hit) {
				onPlatform = true;
				if (behaviour == Behaviour.bouncy) {
					
				
					Bouncing (hit.transform.gameObject);
				}
		

			} 
	}
}
	IEnumerator resetPlatform(){
	
		yield return new WaitForSeconds (2);
		onPlatform = false;
		this.transform.position = oldPos;
		fallingTime = originalFallingtime;
		StopCoroutine (resetPlatform ());
	}
}
