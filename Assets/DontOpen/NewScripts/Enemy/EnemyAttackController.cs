﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttackController : MonoBehaviour {

	public bool nearEnemy =false;
	public  bool isAttacking;
	public float attackTimer;
	public BoxCollider2D attackCollider;
	public float attackPause=1;
	public GameObject sprite;

	void Update(){
		
		if (nearEnemy) {
			

			attackPause -= Time.deltaTime;

			if (!isAttacking && attackPause<=0) {
				attackPause = 2;
				isAttacking = true;
				attackTimer = 20;
				attackCollider.enabled = true;
				sprite.GetComponent<SpriteRenderer> ().enabled = true;

		
			}
			if (isAttacking) {
				
				attackTimer--;
				if (attackTimer <= 0) {

					attackTimer = 0;
					attackCollider.enabled = false;
					sprite.GetComponent<SpriteRenderer> ().enabled = false;
					isAttacking = false;

				}
		
			}
		} else {
		
		
			isAttacking=false;
			attackTimer=0;
			attackCollider.enabled=false;
			sprite.GetComponent<SpriteRenderer> ().enabled = false;
			attackPause=2;
		
		
		}
		}

}
