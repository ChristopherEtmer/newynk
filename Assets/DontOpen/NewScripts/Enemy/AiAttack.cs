﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiAttack : MonoBehaviour {

	public BoxCollider2D myCollider;
	public Vector3 size;
	public LayerMask enemy;
	public bool attacking;
	Vector2 velocity;
	[Range(0,50)]
	public int myDamage;
	public GameObject player;
	public GameObject Enemy;
	public bool seeing;
	public float attacktimer;
	public bool damage;
	public EnemyMover mover;
	public Animator anim;
	public Vector2 distance;
	Bounds bounds;
	public LayerMask groundMask;
	Vector2 left;
	Vector2 right;
	public float dirX;
	public Vector3 pos;
	public float dist;

	// Use this for initialization

	void Start(){
	
		anim = GetComponentInParent<Animator> ();
	
	}
	
	// Update is called once per frame
	void Update () {

			looking ();

	
		if (player != null) {
		
			distance = player.transform.position - Enemy.transform.position;
			mover.dirX = dirX;

		
		}
		dist = distance.magnitude;
		if (distance.magnitude >= 10) {
		
			attacking = false;
		}
		if (seeing) {

			anim.SetTrigger ("Attack");

		}
	
		if (attacking) {
			attack ();
		} else {
		
			damage = false; 
		
		}
	}
	void looking (){
		rayPos ();
		Vector2 GroundCheckPos= (dirX==1)?right:left;

		RaycastHit2D groundCheck = Physics2D.Raycast (GroundCheckPos , Vector2.down, 2, groundMask);
		Debug.DrawRay (GroundCheckPos, Vector2.down * 5, Color.cyan);
		if (!groundCheck) {
			dirX = -dirX;
		} 
			RaycastHit2D lookOut = Physics2D.BoxCast (this.transform.position+pos, size, 0, Vector2.right*dirX, 0, enemy);
			if (lookOut) {
			
				player = lookOut.transform.gameObject;
				seeing = true;
			} else {
				seeing = false;
			}

	}

	void attack(){



		RaycastHit2D hit = Physics2D.BoxCast (this.transform.position+pos, this.transform.localScale, 0, Vector2.right, 0, enemy);

		if (hit) {
			if (!damage) {
				 
				damage = true;
				print ("hit");
				hit.transform.gameObject.GetComponent<Damage> ().underAttack (myDamage);

			} 
		} 
	}
	void OnDrawGizmosSelected(){

		Gizmos.color = Color.red; 
		if (attacking) {
			Gizmos.DrawWireCube (this.transform.position+pos, this.transform.localScale);
		}
		Gizmos.DrawWireCube (this.transform.position+pos, size);


	}
	public void rayPos(){

	 bounds = myCollider.bounds;

	 left = new Vector2(bounds.min.x,bounds.min.y);
	 right = new Vector2(bounds.max.x,bounds.min.y);
	}

}
