﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour {

	public GameObject head;
	public float angle;
	public float rayCount;
	public float raySpacing;
	public float DirectionX;
	public Controller controller;
	public LayerMask enemyMask;
	float hyp;
	public BoxCollider2D attackcollider;
	public float lookingDistance ;
	public EnemyMover mover;
	Vector2 originalVelocity;
	public float attackRadius;
	public float attacktimer;
	public bool isattacking;
	public bool moving= true;
	public Animator anim;
	public Animation animation;
	public Vector2 size;
	public GameObject eye;
	public bool inAttackZone;
	void Start(){
		
		rayUpdate ();
		originalVelocity = mover.velocity;
	}
	void Update(){

		if (attacktimer > 0) {

		
			attacktimer-=Time.deltaTime;

		}
		if (attacktimer <= 0) {
			
			attacktimer = 0;
			anim.SetBool ("Attack", false);
			isattacking = false;
		}


	}
	
	// Update is called once per frame
	public void looking (Vector2 velocity) {
		DirectionX = Mathf.Sign (velocity.x);
		hyp = 5;

		for (int i = 0; i < rayCount; i++) {

			Vector2 rayOrigin = head.transform.position;

			Vector2 lookingDirection;
			lookingDirection.x = Mathf.Cos ((raySpacing) * i * Mathf.Deg2Rad - 30 * Mathf.Deg2Rad) * hyp * DirectionX;
			lookingDirection.y = Mathf.Sin ((raySpacing) * i * Mathf.Deg2Rad - 30 * Mathf.Deg2Rad) * hyp;



			RaycastHit2D hit = Physics2D.Raycast (rayOrigin, lookingDirection, lookingDistance, enemyMask);
		
			if (hit) {
				Debug.DrawRay (rayOrigin, lookingDirection, Color.cyan);
				if (hit.distance <= lookingDistance / 1.5f) {
					SeeingPlayer (hit.transform.gameObject);

					Debug.DrawLine (this.transform.position, hit.transform.position, Color.red);
				


				}

				inAttackZone = Physics2D.OverlapBox (eye.transform.position, size, 0, enemyMask);

				if (inAttackZone) {
					if (!isattacking) {
						
						isattacking = true;
						attacking ();

					}else {

					moving = true;

				} 
			}
		}
	}
	}
	
	public void rayUpdate(){

		raySpacing = 160 / (rayCount - 1);

	}

	public void SeeingPlayer(GameObject player){
	

		moving = false;
		Vector2 playerDistance;
		playerDistance = player.transform.position - head.transform.position;
		//Debug.DrawRay (head.transform.position, playerDistance,Color.red);
		if (Mathf.Sign (playerDistance.x) == Mathf.Sign (controller.dirX)) {
			
				
				controller.dirX = -controller.dirX;	

		
		
		}

		if (inAttackZone) {
			

			mover.velocity.x = 0;


		} 
	}
	public void attacking(){
		anim.SetBool ("Attack", true);

		attacktimer = 1.5f;
		print ("2");

	}

	void OnDrawGizmosSelected(){

		Gizmos.color = Color.blue;
		Gizmos.DrawWireCube (eye.transform.position, size);

	}
}
