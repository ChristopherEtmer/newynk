﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMover : MonoBehaviour {


	public Controller controller;
	public Vector2 velocity;
	public float movingSpeed;
	public float dirX;
	public bool patroling;
	Vector2 looking;
	public GameObject Sprite;
	public AiAttack intelligence;
	public Animator anim;
	// Use this for initialization
	void Start () {
		anim = GetComponentInChildren<Animator> ();
	}
	
	// Update is called once per frame

		void Update(){
		if (controller.collisions.below ) {

			velocity.y = 0;

		}

		velocity.y -= 20*Time.deltaTime;
	
	
		
		

			velocity.x = movingSpeed *intelligence.dirX;

		
			
		

		if(Mathf.Sign(velocity.x)==-1){

			Quaternion rot = Sprite.transform.localRotation;
			rot.y =180;
			Sprite.transform.localRotation =rot;
		}else{

			Quaternion rot = Sprite.transform.localRotation;
			rot.y =0;
			Sprite.transform.localRotation =rot;
		}

		if (intelligence.seeing== false) {
			controller.move (velocity * Time.deltaTime);
		}

		
	

		
	}

}
