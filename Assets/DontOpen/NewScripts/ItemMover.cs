﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemMover : MonoBehaviour {
	public Vector2 velocity;
	public Controller controller;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		controller.move (velocity * Time.deltaTime);
	
		
	}
}
