﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weight : RayCastController {

	// Use this for initialization
	public LayerMask mask;
	public LayerMask passengerMask;
	public BoxCollider2D boxTransform;
	public PlatformController controller;
	public bool isgrounded;
	public GameObject collision;
	public float OverlapSizeX;
	public float OverlapSizeY;
	public Vector2 size;
	public Sprite spriteSize;
	public Vector3 pos;
	// Update is called once per frame
	void Start(){



	}
	public void Move(Vector2 velocity){

	
		RaycastUpdateOrigin ();
		controller.move (velocity);
		VerticalCollision (ref velocity);

		transform.Translate (velocity * Time.fixedDeltaTime);

	}
	void OnDrawGizmosSelected(){

		Gizmos.color = Color.red;
		Gizmos.DrawWireCube (this.transform.position+pos, size);


	}
	void VerticalCollision (ref Vector2 velocity)
	{

		isgrounded = Physics2D.OverlapBox (boxTransform.transform.position+pos,size, 0, mask);

		float directionY = Mathf.Sign (velocity.y);
		float raylength = 5*skinWidth;

		for (int i = 0; i < horizontalRayCount; i++) {
			
			Vector2 rayOrigin =(directionY==-1)?raycastOrigin.bottomLeft:raycastOrigin.topLeft;
			rayOrigin += Vector2.right * (horizontalRaySpacing * i );

			RaycastHit2D hit = Physics2D.Raycast (rayOrigin, Vector2.up*directionY, raylength,mask); 
			Debug.DrawRay (rayOrigin, Vector2.up*raylength*directionY, Color.green);

			if (hit) {
				
				velocity.y = hit.distance + skinWidth;
				raylength = hit.distance;

			} 
			
				
			

		}
	}


}
