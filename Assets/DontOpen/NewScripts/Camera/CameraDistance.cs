﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDistance : MonoBehaviour {

	public LayerMask collision;
	public Camera cam;
	float smoothingVelocity;
	public float smoothingTime;
	float newSize;
	public float addSize;
	bool colliding;
	float originalSize;

	void Awake(){


	}
	void Start(){
		

		originalSize = cam.orthographicSize;
		newSize = cam.orthographicSize + addSize;

	
	
	}

	void OnDrawGizmos(){

		Gizmos.color = Color.cyan;
		Gizmos.DrawWireCube (this.transform.position, new Vector2 (this.transform.localScale.x * 0.2f, this.transform.localScale.y * 0.2f));


	}
	void FixedUpdate(){
		
	
		colliding = Physics2D.OverlapBox (this.transform.position, new Vector2 (this.transform.localScale.x * 0.2f, this.transform.localScale.y * 0.2f), 0, collision);
		if (colliding) {
		
		
			float size = Mathf.SmoothDamp (cam.orthographicSize, newSize, ref smoothingVelocity, smoothingTime);

			cam.orthographicSize = size;

		
		} else {
		
			float size = Mathf.SmoothDamp (cam.orthographicSize,originalSize, ref smoothingVelocity, smoothingTime);

			cam.orthographicSize = size;
		
		
		}
	
	
	}

}
