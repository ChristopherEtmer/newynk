﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayerScript : MonoBehaviour {
	public Vector2 camConstrainX;
	public Vector2 camConstrainY;

	public Vector3 pos;
	public float camFocusX;
	public float camFocusY;
	public Vector2 velocity;
	public float smoothingVelocityX;
	public float smoothingVelocityY;
	public float smoothingTimeeX;
    public float smoothingTimeeY;
	public GameObject player;
	public 	Vector2 cameraMover;
	public Vector2 cameraStick;

	float dir;
	bool turn;
	float originalCamFocusX;
	public float negativCamFocusX;
	float smoothing;
	// Use this for initialization
	void Start () {
		originalCamFocusX = camFocusX;
		negativCamFocusX = -camFocusX;
		}
	
	// Update is called once per frame
	void Update () {
		dir = Input.GetAxis ("LeftJoyStickX");

		if (dir < 0) {
		
			turn = true;
		
		} else if(dir>0) {
		
			turn = false;

		
		}
		if (turn) {
		
			camFocusX = Mathf.SmoothDamp (camFocusX, negativCamFocusX, ref smoothing, 1f);
		
		} else {
		
			camFocusX = Mathf.SmoothDamp (camFocusX,originalCamFocusX, ref smoothing, 1f);
		
		}
		if (player == null) {
			player = GameObject.Find ("Player(Clone)");

		
		}
		cameraStick.x= Input.GetAxis("RightJoyStickX");
		cameraStick.y= Input.GetAxis("RightJoyStickY");

		cameraMover.x = 2f *cameraStick.x;
		cameraMover.y = 2f *cameraStick.y;

		if (player != null) {

			pos.x = player.transform.position.x + camFocusX+cameraMover.x;
			pos.x = Mathf.Clamp (pos.x, camConstrainX.x, camConstrainX.y);
			pos.y = player.transform.position.y + camFocusY+cameraMover.y;
			pos.y = Mathf.Clamp (pos.y, camConstrainY.x, camConstrainY.y);

			float velocityX = Mathf.SmoothDamp (this.transform.position.x, pos.x, ref smoothingVelocityX, smoothingTimeeX);
			float velocityY = Mathf.SmoothDamp (this.transform.position.y, pos.y, ref smoothingVelocityY, smoothingTimeeY);

			transform.position = new Vector3 (velocityX, velocityY, -10);
		}
	
		
	}
}
