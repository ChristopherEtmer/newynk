﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour {
	public GameObject Child;
	public float angle=0;
	public 	Quaternion rot;
	float timer;
	public bool counterclockwise;
	// Use this for initialization
	void Start () {
		
	}
	void Update(){
	
		angle = this.transform.localRotation.z+Child.GetComponent<PlatformMover> ().circleAngle ;
		if (counterclockwise) {
			angle = -angle;
		}
	
		transform.rotation = Quaternion.AngleAxis (angle, Vector3.back);
	}
	// Update is called once per frame

}
