﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pulley : MonoBehaviour {

	public GameObject Weight_1;
	public GameObject Weight_2;

	public Vector2 countervelocity;
	public Vector2 velocity;
	float smoothing;
	public float smoothigTime;

	public float  potEnergy;
	public float  gravity;
	public float  height;



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		
		height = Mathf.Abs((Weight_1.transform.localPosition.y - Weight_2.transform.localPosition.y)/2);
		potEnergy = gravity * height;

		float targetVelocity;

		targetVelocity = gravity * potEnergy*Time.deltaTime;
		velocity.y = Mathf.SmoothDamp (velocity.y, targetVelocity, ref smoothing, smoothigTime);
		if (Weight_1.GetComponent<Weight> ().isgrounded||Weight_2.GetComponent<Weight> ().isgrounded) {
		
			velocity.y = 0;
		
		
		}


		if (Weight_1.transform.localPosition.y < Weight_2.transform.localPosition.y) {
			
			Weight_1.GetComponent<Weight> ().Move (velocity);
			Weight_2.GetComponent<Weight> ().Move (-velocity);

		}
		else if(Weight_1.transform.localPosition.y > Weight_2.transform.localPosition.y){
			
			Weight_1.GetComponent<Weight> ().Move (-velocity);
			Weight_2.GetComponent<Weight> ().Move (velocity);

		}	



	}


}
