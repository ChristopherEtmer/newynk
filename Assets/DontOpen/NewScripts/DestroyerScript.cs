﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyerScript : MonoBehaviour {

	public LayerMask Destroyer;
	public GameObject checkPoint;
	void Start () {
		
	}
	void Update(){
		
		checkPoint= GameObject.FindGameObjectWithTag("currentCheckpoint");
	
	}
	// Update is called once per frame
	void OnTriggerEnter2D(Collider2D col){

		if (col.gameObject.tag=="Player") {
			Destroy(col.gameObject);

		
		}
	

	}
	void OnDrawGizmos(){
	
		Gizmos.color = Color.red;
		Gizmos.DrawWireCube(this.transform.position,new Vector2(this.transform.localScale.x,this.transform.localScale.y));
	
	}
}
