﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileManager : MonoBehaviour {

	public GameObject[] sprite;
	public enum spriteType
	{
		Left,Mid,Right
	};
	public spriteType type;
	// Use this for initialization


	// Update is called once per frame
	void Start () {

		if (type == spriteType.Left) {
		
			SpriteChange (0);

		}
		if (type == spriteType.Mid) {

			SpriteChange (1);

		}
		if (type == spriteType.Right) {

			SpriteChange (2);

		}
	}
	void SpriteChange(int Index){

		for (int i = 0; i < sprite.Length; i++) {
		
			if (i == Index) {
			
				sprite [i].SetActive (true);
			
			} else {
			
				sprite [i].SetActive (false);
			
			}
		
		}
	
	}

}
