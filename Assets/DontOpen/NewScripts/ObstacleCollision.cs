﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleCollision : RayCastController {

	Vector2 velocity;
	public LayerMask mask;
	public LayerMask passengerMask;
	public bool colliding;
	public float x;
	public float y;
	public Controller controller;	// Use this for initialization
	public override void Start ()
	{
		base.Start ();
	}
	
	// Update is called once per frame
	void Update(){
		RaycastUpdateOrigin ();
		colliding = Physics2D.OverlapBox (this.transform.position, new Vector2 (x, y), 0, passengerMask);
	
		if (colliding) {

			horizontalCollision ();

		}
	

	}

	void horizontalCollision(){
		

		float rayLength =4.0f*skinWidth;
		for (int i = 0; i < verticalRayCount; i++) {
			Vector2 rayOriginLeft = raycastOrigin.bottomLeft + Vector2.up * verticalRaySpacing * i;
			RaycastHit2D hitLeft = Physics2D.Raycast (rayOriginLeft,Vector2.left,rayLength,passengerMask);
			RaycastHit2D hitRight = Physics2D.Raycast (raycastOrigin.bottomRight+Vector2.up*verticalRaySpacing*i,Vector2.right,rayLength, passengerMask);
			Debug.DrawRay (rayOriginLeft, Vector2.left * rayLength*10, Color.red);

			if (hitRight) {


				hitLeft.transform.Translate (new Vector2(rayLength, 0));


			}

			if (hitLeft) {


				hitLeft.transform.Translate (new Vector2 (-rayLength, 0));



			} 
		}


	}
	void OnDrawGizmosSelected(){
	
		Gizmos.color = Color.red;
		Gizmos.DrawWireCube (this.transform.position, new Vector2 (x, y));
	
	}

}
