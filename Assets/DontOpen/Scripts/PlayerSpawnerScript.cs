﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawnerScript : MonoBehaviour {
	public GameObject Player;
	// Use this for initialization
	void Start () {
		Instantiate (Player, this.transform.position, Quaternion.identity);
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
