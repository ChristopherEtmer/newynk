﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerControllerScript : MonoBehaviour {


	public int dirX;
	public Rigidbody2D rb;
	public Vector2 pos;
	public Transform circlePos;
	public LayerMask layer;
	public BoxCollider2D playerSize;
	public Vector2 playerOriginalSize;
	public Vector2 playerNewSize;
	public Vector2 originalOffset;
	public BoxCollider2D[] weapons;
	public GameObject[] Sprites;
	public GameObject bullets; 
	public GameObject bulletSpawner;
	public int weaponSlot;
	public bool isattacking;
	public int attackTimer;
	public bool isgrounded;
	public float circleRadius; 
	public float dir;
	public float speed = 200;
	public float jumpSpeed = 200;

	public bool facingRight = true;


	// Use this for initialization
	void Start () {
		playerSize = GetComponent<BoxCollider2D> ();
		playerOriginalSize = new Vector2(playerSize.size.x,playerSize.size.y);
		playerNewSize= new Vector2(playerSize.size.x,playerSize.size.y/2);
		originalOffset=playerSize.offset;


	}
	
	// Update is called once per frame
	void Update () {
		

		isgrounded = Physics2D.OverlapCircle (circlePos.position, circleRadius, layer);

	


		pos = this.transform.position;

		dir = Input.GetAxis ("Horizontal");

		if(Input.GetKeyDown(KeyCode.Alpha1))
			{
			if (weaponSlot < weapons.Length) {
				weaponSlot++;

			}

				
		}else if (Input.GetKeyDown(KeyCode.Alpha2))
				{
			if (weaponSlot > 0) {
				weaponSlot--;
			
			}
				}

		if ( Input.GetKeyDown(KeyCode.UpArrow) && isgrounded)
		{
			rb.AddForce (Vector2.up * jumpSpeed,ForceMode2D.Impulse);
		 
		}
		if (Input.GetKey (KeyCode.DownArrow)) {
			playerSize.size = playerNewSize;
			pos.y = this.transform.position.y / 2;
			Sprites [0].SetActive (false);
			Sprites [2].SetActive (true);


		} else {
		
			playerSize.size = playerOriginalSize;
			pos.y = this.transform.position.y *2;
			Sprites [0].SetActive (true);
			Sprites [2].SetActive (false);
		
		}

		if(Input.GetKeyDown(KeyCode.F)&& !isattacking)
			{

				isattacking = true;

			    Sprites [0].SetActive (false);
			    Sprites [1].SetActive (true);
			    attackTimer = 5;
			if (weaponSlot < 2) {
				weaponColliderActivate (weaponSlot);
			
			} else {
				
				Instantiate (bullets, bulletSpawner.transform.position, this.transform.rotation);

			
			}

			}

			if(isattacking)
			{

			attackTimer--;


			}
		if (attackTimer < 0) {
			
			isattacking = false;
			Sprites [0].SetActive (true);
			Sprites [1].SetActive (false);

			if (weaponSlot < 2) {
				weapons [weaponSlot].enabled = false;	

			} 
		}
		if ( Input.GetKey(KeyCode.LeftArrow))
			{
			rb.AddForce( Vector2.left*speed,ForceMode2D.Impulse);
			facingRight = false;
			}
		else if( Input.GetKey(KeyCode.RightArrow))
				{
			
			rb.AddForce( Vector2.right*speed,ForceMode2D.Impulse);
			facingRight = true;
					
		
	}



		if (!facingRight) {
		
			Quaternion rot = transform.localRotation;
			rot.y = 180;
			transform.localRotation = rot;
			dirX = -1;

		} else {

			Quaternion rot = transform.localRotation;
			rot.x = 0;
			rot.y = 0;
			transform.localRotation = rot;
			dirX = 1;
		}


}

	public void weaponColliderActivate (int index)
	{
		for (int i = 0; i < weapons.Length; i++) {
			if (i == index) {
				weapons [i].enabled = true;

			} else {
				weapons [i].enabled = false;

			}
		}
	} 
				}
