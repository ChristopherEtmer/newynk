﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteractionScript : MonoBehaviour {

	public GameObject player;
	public bool wallJump;
	public GameObject item;
	public BoxCollider2D interactionCollider;
	public bool isholding =false;
	public bool holdingCol = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (item == null) {
		
			Debug.Log ("notHolding");
		}

		if (Input.GetKey (KeyCode.E) && !isholding&& holdingCol) {

			isholding = true;

		} else if (Input.GetKeyDown (KeyCode.E) && isholding) {
	
			isholding = false;
			holdingCol = false;

			item= null;
		
		}
		if (isholding) {
		
			item.gameObject.transform.position = interactionCollider.transform.position;
		
		}
		
	}
	void OnTriggerEnter2D (Collider2D col){
	
		if (col.gameObject.name == "Item") {
		
			holdingCol = true;
			item =col.gameObject;
		
		}

	}
	
		void OnTriggerExit2D (Collider2D col){

			if (col.gameObject.name == "Item") {

				holdingCol =  false;
				

		}



		}
}
