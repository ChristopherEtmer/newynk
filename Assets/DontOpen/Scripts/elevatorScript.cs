﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class elevatorScript : MonoBehaviour {
	public Player player;
	public Vector2 originalPos;
	public Vector2 pos;
	public int dirY =1;
	public float speed = 1f;
	public float amplitude=3;

	// Use this for initialization
	void Start () {

		originalPos =this.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		
		pos = this.transform.position;
		this.transform.Translate( new Vector3 (0f,dirY *speed,0f));


		if (pos.y > originalPos.y + amplitude) {
			pos.y = originalPos.y + amplitude;
			dirY = -1;
		} else if (pos.y < originalPos.y - amplitude) {
			pos.y = originalPos.y - amplitude;
			dirY = 1;
		
		}
		
	}
	void OnTriggerEnter2D(Collider2D col){
	
		if (col.gameObject.name == "Player") {
			player = col.gameObject.GetComponent<Player> ();
			player.velocity += pos;
		
		
		}
	
	}
}
