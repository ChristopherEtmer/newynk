﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingPlatformScript : MonoBehaviour {
	public Vector2 pos;
	public Vector2 originalPos;
	public GameObject platform;
	public float fallingSpeed = 1f;
	public float dissapearingSpeed = 4f;
	public bool isFalling =false;
	public float fallingTimer=2f;
	public float respawnTimer =3f;
	// Use this for initialization
	void Start () {
		
		
	}
	void Awake(){
	
		originalPos = platform.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		pos = platform.transform.position;

		if(isFalling){

			pos.y = pos.y - 0.1f * fallingSpeed;
			platform.transform.position = pos;
			if (pos.y < originalPos.y - dissapearingSpeed) {
				platform.gameObject.SetActive (false);
				isFalling =false;
				StartCoroutine (respawn ());

		}

	}
	}
	void OnTriggerEnter2D (Collider2D col){
	
		if (col.gameObject.name=="Player(Clone)") {
			Debug.Log ("Klappt");
			StartCoroutine (falling ());
		
		}
	
	
	}

	IEnumerator falling()
	{
		yield return new WaitForSeconds (fallingTimer);
		isFalling = true;
		StopCoroutine (falling ());


	}
	IEnumerator respawn(){
	
		yield return new WaitForSeconds (respawnTimer);
		platform.gameObject.SetActive (true);
		platform.transform.position = originalPos;
		StopCoroutine (respawn ());
	
	}
	
	}

