﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlowScript : MonoBehaviour {
	public CircleCollider2D myCollider;
	public LayerMask enemy;
	[Range(0,50)]
	public int myDamage;
	bool tookDamage;
	// Use this for initialization
	void Start () {
		tookDamage = false;
	}
	
	// Update is called once per frame
	void Update () {

		RaycastHit2D hit = Physics2D.CircleCast (this.transform.position, myCollider.radius * this.transform.localScale.x, Vector2.right, 0, enemy);
		if (hit) {
			if (!tookDamage) {
				hit.transform.gameObject.GetComponent<Damage> ().underAttack (myDamage);
				tookDamage = true;
			}
		
		
		}
	}
}
