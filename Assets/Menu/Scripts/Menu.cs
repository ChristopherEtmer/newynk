﻿using UnityEngine;
using System.Collections;

public class Menu : PauseInheritance {


	// Use this for initialization
	void Start () {
		Time.timeScale = 1;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyUp(KeyCode.Escape) && !isPaused){
			PauseGame();
		}
		else if(Input.GetKeyUp(KeyCode.Escape) && isPaused){
			UnpauseGame();
		}
	}
	
	public void PauseGame(){
		isPaused = true;
		Time.timeScale = 0;
		Application.LoadLevelAdditive (1);
	}
	
	public void UnpauseGame(){
		isPaused = false;
		Time.timeScale = 1;
		Destroy(GameObject.Find("Menu"));
	}

	public void LoadMainMenu(){
		Application.LoadLevel (0);
	}

	public void QuitGame(){
		Application.Quit ();
	}
}
