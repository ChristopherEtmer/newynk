﻿using UnityEngine;
using System.Collections;

public class SwitchMenu : MonoBehaviour {
	public GameObject Previous;
	public GameObject Next;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	
	public void DisplayNext() {
		Previous.SetActive (true);
		Next.SetActive (true);
	}
}
