﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

	public string nextScene;
	public LayerMask player;


	// Use this for initialization

	
	// Update is called once per frame
	void Update () {

		RaycastHit2D hit = Physics2D.BoxCast (this.transform.position,this.transform.localScale,0,Vector2.right,0,player);
		if (hit) {
			
			SceneManager.LoadScene (nextScene,LoadSceneMode.Single);
		
		}

	}
	void OnDrawGizmosSelected(){
	
		Gizmos.color = Color.blue;
		Gizmos.DrawWireCube (this.transform.position, this.transform.localScale);
	
	}
}
