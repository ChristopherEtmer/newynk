﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : StatScript {
	public CheckpointScript checkcontrol;
	public int player;
	BoxCollider2D thisCollider;
	public LayerMask damageMask;
	public enum Type{Player,Enemy,Obstacle, Boss}
	public Type type;
	public Sprite[] DecayStates;
	public int index;
	// Use this for initialization
	void Start () {
		thisCollider = this.gameObject.GetComponent<BoxCollider2D> ();

		if (this.gameObject.tag == "Player") {

			player = 0;
		}
		if (this.gameObject.tag == "Player2") {
			
			player = 1;
		}
	
	}
	
	// Update is called once per frame
	void Update () {

	
		if (Health <= 0) {
			if (type == Type.Player) {
				checkcontrol = GameObject.FindWithTag ("currentCheckpoint").GetComponent<CheckpointScript> ();
				checkcontrol.Respawn ();
			}
			if (type == Type.Enemy || type == Type.Obstacle || type == Type.Player) {
				Destroy (this.gameObject);

			}

		}
	}
	public void underAttack(int damage){
		if (type != Type.Obstacle) {
			
			takeDamage (damage);

		}else if(type==Type.Obstacle){
			index = Mathf.Clamp (index, 0, 3);
			    index++;
				takeDamage(damage/damage);
			GetComponentInChildren<SpriteRenderer> ().sprite = DecayStates [index];

		}
	

		}

		
			

		


}
